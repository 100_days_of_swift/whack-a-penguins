//
//  GameScene.swift
//  Whack-A-Penguins
//
//  Created by Hariharan S on 06/06/24.
//

import SpriteKit

class GameScene: SKScene {
    var numRounds = 0
    var popupTime = 0.85
    var slots = [WhackSlot]()
    var gameScore: SKLabelNode!
    var score = 0 {
        didSet {
            self.gameScore.text = "Score: \(score)"
        }
    }
    
    override func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "whackBackground")
        background.position = CGPoint(x: 512, y: 384)
        background.blendMode = .replace
        background.zPosition = -1
        self.addChild(background)
        
        self.gameScore = SKLabelNode(fontNamed: "Chalkduster")
        self.gameScore.text = "Score: 0"
        self.gameScore.position = CGPoint(x: 8, y: 8)
        self.gameScore.horizontalAlignmentMode = .left
        self.gameScore.fontSize = 48
        self.addChild(self.gameScore)
        
        for i in 0 ..< 5 {
            self.createSlot(
                at: CGPoint(
                    x: 100 + (i * 170),
                    y: 410
                )
            )
        }
        for i in 0 ..< 4 {
            self.createSlot(
                at: CGPoint(
                    x: 180 + (i * 170), 
                    y: 320
                )
            )
        }
        for i in 0 ..< 5 {
            self.createSlot(
                at: CGPoint(
                    x: 100 + (i * 170),
                    y: 230
                )
            )
        }
        for i in 0 ..< 4 {
            self.createSlot(
                at: CGPoint(
                    x: 180 + (i * 170),
                    y: 140
                )
            )
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.createEnemy()
        }
    }

    override func touchesBegan(
        _ touches: Set<UITouch>,
        with event: UIEvent?
    ) {
        guard let touch = touches.first
        else {
            return
        }
            let location = touch.location(in: self)
            let tappedNodes = nodes(at: location)

        for node in tappedNodes {
            guard let whackSlot = node.parent?.parent as? WhackSlot
            else {
                continue
            }
            if !whackSlot.isVisible {
                continue
            }
            if whackSlot.isHit {
                continue
            }
            whackSlot.hit()

            if node.name == "charFriend" {
                self.score -= 5
                self.run(
                    SKAction.playSoundFileNamed(
                        "whackBad.caf",
                        waitForCompletion: false
                    )
                )
            } else if node.name == "charEnemy" {
                whackSlot.charNode.xScale = 0.85
                whackSlot.charNode.yScale = 0.85
                self.score += 1
                self.run(
                    SKAction.playSoundFileNamed(
                        "whack.caf",
                        waitForCompletion: false
                    )
                )
            }
        }
    }
    
    func createEnemy() {
        self.numRounds += 1

        if self.numRounds >= 30 {
            for slot in self.slots {
                slot.hide()
            }

            let gameOver = SKSpriteNode(imageNamed: "gameOver")
            gameOver.position = CGPoint(x: 512, y: 384)
            gameOver.zPosition = 1
            self.addChild(gameOver)
            return
        }
        
        self.popupTime *= 0.991

        self.slots.shuffle()
        self.slots[0].show(hideTime: popupTime)

        if Int.random(in: 0...12) > 4 { slots[1].show(hideTime: popupTime) }
        if Int.random(in: 0...12) > 8 {  slots[2].show(hideTime: popupTime) }
        if Int.random(in: 0...12) > 10 { slots[3].show(hideTime: popupTime) }
        if Int.random(in: 0...12) > 11 { slots[4].show(hideTime: popupTime)  }

        let minDelay = popupTime / 2.0
        let maxDelay = popupTime * 2
        let delay = Double.random(in: minDelay...maxDelay)

        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
            self?.createEnemy()
        }
    }
    
    func createSlot(at position: CGPoint) {
        let slot = WhackSlot()
        slot.configure(at: position)
        self.addChild(slot)
        self.slots.append(slot)
    }
}
