//
//  WhackSlot.swift
//  Whack-A-Penguins
//
//  Created by Hariharan S on 06/06/24.
//

import SpriteKit

class WhackSlot: SKNode {
    var isHit = false
    var isVisible = false
    var charNode: SKSpriteNode!
    
    func configure(at position: CGPoint) {
        self.position = position

        let sprite = SKSpriteNode(imageNamed: "whackHole")
        self.addChild(sprite)
        
        let cropNode = SKCropNode()
        cropNode.position = CGPoint(x: 0, y: 15)
        cropNode.zPosition = 1
        cropNode.maskNode = SKSpriteNode(imageNamed: "whackMask")

        self.charNode = SKSpriteNode(imageNamed: "penguinGood")
        self.charNode.position = CGPoint(x: 0, y: -90)
        self.charNode.name = "character"
        cropNode.addChild(self.charNode)
        self.addChild(cropNode)
    }
    
    func show(hideTime: Double) {
        if self.isVisible {
            return
        }

        self.charNode.xScale = 1
        self.charNode.yScale = 1
        self.charNode.run(
            SKAction.moveBy(
                x: 0,
                y: 80,
                duration: 0.05
            )
        )
        self.isVisible = true
        self.isHit = false

        if Int.random(in: 0...2) == 0 {
            self.charNode.texture = SKTexture(imageNamed: "penguinGood")
            self.charNode.name = "charFriend"
        } else {
            self.charNode.texture = SKTexture(imageNamed: "penguinEvil")
            self.charNode.name = "charEnemy"
        }
        
        DispatchQueue.main.asyncAfter(
            deadline: .now() + (hideTime * 3.5)) { [weak self] in
            self?.hide()
        }
    }
    
    func hide() {
        if !self.isVisible {
            return
        }

        self.charNode.run(SKAction.moveBy(x: 0, y: -80, duration: 0.05))
        self.isVisible = false
    }
    
    func hit() {
        self.isHit = true

        let delay = SKAction.wait(forDuration: 0.25)
        let hide = SKAction.moveBy(x: 0, y: -80, duration: 0.5)
        let notVisible = SKAction.run { [unowned self] in
            self.isVisible = false
        }
        self.charNode.run(
            SKAction.sequence([delay, hide, notVisible])
        )
    }
}
